<?php

Route::group(['middleware' => 'web', 'prefix' => 'galeria', 'namespace' => 'Modules\Gallery\Http\Controllers'], function()
{
    Route::get('listar', 'GalleryController@all')->name('gallery.all');
    Route::get('criar', 'GalleryController@create')->name('gallery.create');
    Route::post('criar', 'GalleryController@store')->name('gallery.store');
    Route::get('editar/{slug}', 'GalleryController@edit')->name('gallery.edit');
    Route::put('editar/{slug}', 'GalleryController@update')->name('gallery.update');
    Route::get('situacao/{slug}/{action}', 'GalleryController@situation')->name('gallery.situation');
    Route::get('excluir/{slug}', 'GalleryController@destroy')->name('gallery.destroy');
    Route::get('imagens/{slug}', 'GalleryController@image')->name('gallery.image');

    Route::post('imagens/editar/{id}/{slug}', 'ImageController@edit')->name('image.edit');
	Route::get('imagens/excluir/{id}/{slug}', 'ImageController@destroy')->name('image.destroy');
	Route::get('imagens/destaque/{id}/{slug}', 'ImageController@highlight')->name('image.highlight');
	Route::post('upload/{slug}', 'ImageController@postUpload')->name('upload.post');
});

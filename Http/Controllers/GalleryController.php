<?php

namespace Modules\Gallery\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Collective\Html\HtmlFacade;
use Modules\Image\Entities\Image;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
Use Illuminate\Support\Facades\Input;
use Modules\Gallery\Entities\Gallery;
use Modules\Gallery\Http\Requests\GalleryRequest as Request;

class GalleryController extends Controller
{
    public function __construct(Gallery $galleriesModel, Image $imageModel)
    {
        $this->imageModel = $imageModel;
        $this->galleriesModel = $galleriesModel;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('gallery::index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function all()
    {
        $listFields = [
            'createdDate' => 'Data',
            'name' => 'Nome',
            'image' => 'Fotos',
            'situation' => 'Situação'
        ];

        $data = $this->galleriesModel->get();
        $controller = 'gallery';
        $title = 'Galeria de fotos';

        $btnGroup = [
            '<a href="'.route('gallery.create').'" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Cadastrar nova</a>'
        ];

        $breadcrumb = [$title];

        return view('admin.layouts.list', compact('data', 'title', 'listFields', 'controller', 'btnGroup', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $listFields = [
            'createdDate' => 'Data',
            'name' => 'Nome',
            'image' => 'Fotos',
            'situation' => 'Situação'
        ];

        $dataList = $this->galleriesModel->get();
        $title = 'Cadastrar Galeria';
        $routeForm = 'gallery.create';
        $controller = 'gallery';

        $btnGroup = [
            '<a href="'.route('gallery.all').'" class="btn btn-default btn-sm"><i class="fa fa-arroe-left"></i> Voltar</a>'
        ];

        $breadcrumb = [
            HtmlFacade::link(route('gallery.all'), 'Galeria de fotos'),
            $title
        ];

        return view('gallery::form', compact('listFields', 'dataList', 'title', 'routeForm', 'controller', 'btnGroup', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $gallery = $this->galleriesModel->fill($input);

        if ($gallery->save()) {
            return redirect()->route('gallery.image', $gallery->slug)->with('success', 'Galeria cadstrada com sucesso!');
        }
        return back()->with('fail', 'Falha ao cadastrar a galeria.');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('gallery::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($slug)
    {
        $listFields = [
            'createdDate' => 'Data',
            'name' => 'Nome',
            'image' => 'Fotos',
            'situation' => 'Situação'
        ];

        $dataList = $this->galleriesModel->get();
        $data = $this->galleriesModel->where('slug', $slug)->first();

        if (!$data) {
            abort(404);
        }

        $title = 'Editar galeria: '.$data->name;
        $controller = 'gallery';
        $routeForm = ['gallery.update', $data->slug];

        $btnGroup = [
            '<a href="'.route('gallery.create').'" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Cadastrar nova</a>',
            '<a href="'.route('gallery.all').'" class="btn btn-default btn-sm"><i class="fa fa-arroe-left"></i> Voltar</a>'
        ];

        $breadcrumb = [
            HtmlFacade::link(route('gallery.all'), 'Galeria de fotos'),
            $title
        ];

        return view('gallery::form', compact('listFields', 'dataList', 'data', 'title', 'controller', 'routeForm', 'btnGroup', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $slug)
    {
        $input = $request->all();
        $gallery = $this->galleriesModel->where('slug', $slug)->first();

        if (!$gallery) {
            abort(404);
        }

        $gallery->slug = null;

        if ($gallery->update($input)) {
            return redirect()->route('gallery.edit', $gallery->slug)->with('success', 'Galeria editada com sucesso!');
        }
        return redirect()->route('gallery.edit', $gallery->slug)->with('fail', 'Falha ao editar a galeria.');
    }

    public function image($slug)
    {
        $gallery = $this->galleriesModel->where('slug', $slug)->first();


        if(!$gallery->first()) {
            abort(404);
        }

        $title = 'Imagens da galeria: '.$gallery->name;
        $route = 'upload.post';
        $data = $this->galleriesModel->find($gallery->id)->images()->paginate(10);

        $btnGroup = [
            '<a href="'.route('gallery.create').'" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Cadastrar nova</a>',
            '<a href="'.route('gallery.all').'" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Voltar</a>'
        ];

        $breadcrumb = [
            HtmlFacade::link(route('gallery.all'), 'Galerias de fotos'),
            $title
        ];

        return view('gallery::upload', compact('slug', 'data', 'title', 'route', 'gallery', 'btnGroup', 'breadcrumb'));
    }

    public function situation($slug, $action)
    {
        $gallery = $this->galleriesModel->where('slug', $slug)->first();

        if(!$gallery) {
            abort(404);
        }

        $gallery->situation = $action;

        if ($gallery->save()) {
            return redirect()->back()->with('success', 'Situação do registro alterada com sucesso!');
        }

        return redirect()->back()->with('fail', 'Falha ao alterar a situação do registro.');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($slug)
    {
        $gallery = $this->galleriesModel->where('slug', $slug)->first();

        if(!$gallery) {
            abort(404);
        }

        if ($gallery->delete()) {
            return redirect()->route('gallery.all')->with('success', 'Galeria excluída com sucesso!');
        }

        return redirect()->back()->with('fail', 'Falha ao excluir a galeria.');
    }
}

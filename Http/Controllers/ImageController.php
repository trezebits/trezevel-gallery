<?php

namespace Modules\Gallery\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Image\Entities\Image;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Gallery\Entities\Gallery;
use Modules\Gallery\Repositories\ImageRepository;

class ImageController extends Controller
{
    public function __construct(ImageRepository $imageRepository, Image $imageModel, Gallery $galleriesModel)
    {
        $this->image = $imageRepository;
        $this->imageModel = $imageModel;
        $this->galleriesModel = $galleriesModel;
    }

    public function postUpload($slug)
    {
        $photo = Input::all();
        $response = $this->image->upload($photo, $slug);
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        $input = $request->all();

        $data = $this->imageModel->where('id', $id)->first();
        $data->alt = $input['name'];

        if ($data->update()) {
            return redirect()->route('gallery.image', $slug)->with('success', 'Imagem renomeada com sucesso');
        }

        return redirect()->route('gallery.image', $slug)->with('fail', 'Falha ao renomar a imagem');
    }


    public function highlight($id, $slug) {
        $gallery = $this->galleriesModel->where('slug', $slug)->first();
        $gallery->image_id = $id;

        if ($gallery->update()) {
            return redirect()->route('gallery.image', $slug)->with('success', 'Destaque alterado com sucesso');
        }

        return redirect()->route('gallery.image', $slug)->with('fail', 'Falha ao alterar o destaque');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
        $gallery = $this->galleriesModel->where('slug', $slug)->first();

        if ($id == $gallery->image_id) {
            $img = $this->galleriesModel->find($gallery->id)->images()->where('id', $id)->first();
            $gallery->image_id = $img->id;
            $gallery->update();
        }

        $data = $this->imageModel->where('id', $id)->first();

        if ($data->delete()) {
            return redirect()->route('gallery.image', $slug)->with('success', 'Imagem excluída com sucesso');
        }

        return redirect()->route('gallery.image', $slug)->with('fail', 'Falha ao excluír a imagem');
    }
}

<?php

namespace Modules\Gallery\Repositories;

use Modules\Image\Entities\Image;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Modules\Gallery\Entities\Gallery;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ImageRepository
{
    public function upload($form_data, $gallery)
    {
        $validator = Validator::make($form_data, Image::$rules, Image::$messages);

        if ($validator->fails()) {

            return Response::json([
                'error' => true,
                'message' => $validator->messages()->first(),
                'code' => 400
            ], 400);

        }

        $photo = $form_data['file'];

        $extension = Input::file('file')->getClientOriginalExtension();

        $fileName = rand(111111111111111, 999999999999999).'.'.$extension; // renameing image
        $upload = new ImageManager();
        $upload->make(Input::file('file'))->save(storage_path('app').'/public/'.$fileName);

        if (Input::file('file')->isValid()) {
            $image = new Image;
            $image->src = $fileName;
            $image->ext = $extension;
            $image->alt = $fileName;
            $image->save();

            $galleries = new Gallery;
            $img_id = Gallery::where('slug', $gallery)->first();
            if($img_id->image_id == null) {
                $img_id->image_id = $image->id;
                $img_id->update();
            }

            Gallery::where('slug', $gallery)->first()->images()->attach($image->id);
        }

        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);

    }

    public function delete($originalFilename)
    {
        $full_size_dir = Config::get('images.full_size');
        $icon_size_dir = Config::get('images.icon_size');

        $sessionImage = Image::where('src', 'like', $originalFilename)->first();


        if(empty($sessionImage))
        {
            return Response::json([
                'error' => true,
                'code'  => 400
            ], 400);

        }

        $full_path1 = $full_size_dir . $sessionImage->filename . '.jpg';
        $full_path2 = $icon_size_dir . $sessionImage->filename . '.jpg';

        if ( File::exists( $full_path1 ) )
        {
            File::delete( $full_path1 );
        }

        if ( File::exists( $full_path2 ) )
        {
            File::delete( $full_path2 );
        }

        if( !empty($sessionImage))
        {
            $sessionImage->delete();
        }

        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);
    }
}
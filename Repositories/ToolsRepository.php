<?php

namespace Quati\Repositories;

use Input;
use Quati\File;
use Quati\Page;
use Quati\Image;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ToolsRepository
{
    public function prep_url($value)
    {
        $url = parse_url($value);

        if ( ! $url OR ! isset($url['scheme']))
        {
            return 'http://'.$value;
        }

        return $value;
    }

    /**
     * file and image upload
     * @param  request $form_data form request
     * @param  string $type      type of upload(image or file)
     * @param  string $slug      page slug for relationchip
     * @param  string $aux       name of upload field in form
     * @return integer           image id or true
     */
    public function upload($form_data, $type='', $aux='')
    {
        $validator = Validator::make($form_data, ($type == 'image' ? Image::$rules : File::$rules));

        if ($validator->fails()) {
            return Response::json([
                'error' => true,
                'code' => 400
            ], 400);
        }

        $file = $form_data[(!empty($aux) ? $aux : 'file')];

        $destinationPath = storage_path('app').'/public'; // upload path
        $extension = Input::file((!empty($aux) ? $aux : 'file'))->getClientOriginalExtension(); // getting image extension
        $fileName = rand(111111111111111, 999999999999999).'.'.$extension; // renameing image
        if ($type == 'image') {
            $upload = new ImageManager();
            $upload->make(Input::file((!empty($aux) ? $aux : 'file')))->save($destinationPath.'/'.$fileName);
            if (Input::file((!empty($aux) ? $aux : 'file'))->isValid()) {
                $imagem = new Image;
                $imagem->src = $fileName;
                $imagem->ext = $extension;
                $imagem->alt = $fileName;
                $imagem->save();

                return $imagem->id;
            }
        } elseif($type == 'file') {
            Input::file((!empty($aux) ? $aux : 'file'))->move($destinationPath, $fileName);// uploading file to given path
            $file = new File;
            $file->name = $fileName;
            $file->ext = $extension;
            $file->description = $fileName;
            $file->save();

            return $file->id;
        }
    }

    public function upload_files($file, $name = false)
    {
        $destinationPath = storage_path('app').'/public';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(111111111111111, 999999999999999).'.'.$extension;

        if($file->isValid()) {
            $file->move($destinationPath, $fileName);

            if ($name) {
                return $destinationPath.'/'.$fileName;
            }

            $file = new File;
            $file->name = $fileName;
            $file->ext = $extension;
            $file->description = $fileName;
            $file->save();

            return $file->id;
        }
    }

    public function uploadFilesPage($form_data, $type='', $slug='')
    {
        $destinationPath = storage_path('app').'/public';
        $file = $form_data['file'];
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(111111111111111, 999999999999999).'.'.$extension;

        if($file->isValid()) {
            Input::file('file')->move($destinationPath, $fileName);
            $file = new File;
            $file->name = $fileName;
            $file->ext = $extension;
            $file->description = $fileName;
            $file->save();

            Page::where('slug', $slug)->first()->files()->attach($file->id);

            return Response::json([
                'error' => false,
                'code'  => 200
            ], 200);
        }
    }

    /**
     * get img tag in ckeditor description
     * @param  string   $description text of description
     * @return string   img tag or false
     */
    public function get_img_ckeditor($description) {
        preg_match_all('/<img[^>]+>/i',$description, $result);
        if (!empty($result[0][0])) {
            preg_match_all('/(alt|title|src)=("[^"]*")/i',$result[0][0], $img);
            if(!empty($result[0][0])) {
                $src = explode('"', $img[0][1]);
                return $src[1];
            }
            return false;
        }
        return false;
    }
}
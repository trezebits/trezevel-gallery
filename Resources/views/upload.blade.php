@extends('admin.layouts.app')

@section('content')
    <script src="{{ mix('js/dropzone-config.js') }}"></script>
    <script src="{{ mix('js/dropzone.js') }}"></script>
    <link rel="stylesheet" href="{{ mix('css/dropzone.css') }}">
    @include('admin.layouts.partials.alerts')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    {!! Form::open(['url' => route($route, $slug), 'class' => 'dropzone dropzone-ueti', 'files' => true, 'id' => 'real-dropzone']) !!}
                        {{ csrf_field() }}
                        <div class="dz-message">
                            <h4 style="text-align: center;color:#428bca;">
                                Arraste os arquivos ou clique na caixa para fazer o upload
                            </h4>
                        </div>
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                        <div class="dropzone-previews" id="dropzonePreview"></div>
                    {!! Form::close() !!}
                    <div class="well">
                        <ul>
                            @if(isset($dataFile))
                                <li>Suportadas imagens nos formatos .pdf, .xls, .xlsx, .doc, .docx, .odt, .ppt, .pptx, .rar, .zip, .png, .gif, .jpeg, .jpg, .bmp.</li>
                            @else
                                <li>Suportadas imagens nos formatos .png, .gif, .jpeg, .jpg e .bmp.</li>
                            @endif
                            <li>Tamanho máximo de upload é de 8MB</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-default">
                @if ((isset($data) && sizeof($data) > 0) || (isset($dataFile) && sizeof($dataFile) > 0))
                    @if(isset($dataFile))
                        {!! $data = $dataFile !!}
                    @endif
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>{{ (isset($dataFile) ? 'Data de cadastro' : 'Fotos') }}</th>
                                <th>Descrição</th>
                                @if(isset($dataFile))
                                    <th>Extensão</th>
                                @endif
                                <th>Cadastro</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $row)
                                <tr>
                                    <td>
                                        @if(isset($dataFile))
                                            {{ dateTimeFormatSwitch($row->created_at) }}
                                        @else
                                            <img src="{{ url('images/100_100/'.$row->src) }}" alt="{{ $row->alt }}">
                                        @endif
                                    </td>
                                    <td>
                                        {!! Form::open(['method' => 'POST', 'class' => 'form-inline', 'url' => route((isset($dataFile) ? 'file.edit' : 'image.edit'), [$row->id, $slug])]) !!}
                                            <div class="form-group @if($errors->first('name')) has-error @endif">
                                                {!! Form::label('name', 'Nome:') !!}
                                                {!! Form::text('name', (isset($dataFile) ? $row->description : $row->alt), ['class' => 'form-control']) !!}
                                                <small class="text-danger">{{ $errors->first('name') }}</small>
                                            </div>
                                            <div class="form-group btn-group">
                                                {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
                                            </div>
                                        {!! Form::close() !!}
                                    </td>
                                    @if(isset($dataFile))
                                        <td>
                                            {{ '.'.$row->ext }}
                                        </td>
                                    @endif
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route((isset($dataFile) ? 'file' : 'image').'.destroy', [$row->id, $slug]) }}" class="btn btn-danger btn-sm modal-delete"><i class="fa fa-trash fa-fw"></i></a>
                                             <a href="{{ url('download/'.(isset($dataFile) ? $row->name : $row->src))}} " target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-download fa-fw"></i></a>
                                             @if (!isset($dataFile))
                                                <a href="{{ route('image.highlight', [$row->id, $slug])}}" class="btn {!! (($row->id == $gallery->image_id) ? 'btn-success' : 'btn-default') !!} btn-sm"><span class="fa fa-star fa fw"></span></a>
                                             @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="paginate text-center">
                        {!! $data->render() !!}
                    </div>
                @else
                    @if(isset($dataFile))
                        {!! $data = $dataFile !!}
                    @endif
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>{{ (isset($dataFile) ? 'Data de cadastro' : 'Fotos') }}</th>
                                <th>Descrição</th>
                                @if(isset($dataFile))
                                    <th>Extensão</th>
                                @endif
                                <th>Cadastro</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="{{ sizeof($data)+3 }}">
                                    Nenhum registro encontrado.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>


    <!-- Dropzone Preview Template -->
    <div id="preview-template" style="display: none;">

        <div class="dz-preview dz-file-preview">
            <div class="dz-image"><img data-dz-thumbnail=""></div>

            <div class="dz-details">
                <div class="dz-size"><span data-dz-size=""></span></div>
                <div class="dz-filename"><span data-dz-name=""></span></div>
            </div>
            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
            <div class="dz-error-message"><span data-dz-errormessage=""></span></div>

            <div class="dz-success-mark">
                <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                    <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                    <title>Check</title>
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                    </g>
                </svg>
            </div>

            <div class="dz-error-mark">
                <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                    <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                    <title>error</title>
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                            <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('asset/css/dropzone.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('asset/js/dropzone.js') }}"></script>
    <script src="{{ asset('asset/js/dropzone-config.js') }}"></script>
@endsection
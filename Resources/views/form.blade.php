@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.partials.alerts')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
				@if (isset($data))
		            {!! Form::model($data, ['route' => $routeForm, 'method' => 'PUT', 'class' => 'form']) !!}
		        @else
		            {!! Form::open(['method' => 'POST', 'class' => 'form', 'route' => $routeForm]) !!}
		        @endif
			        <div class="box-body">
			            <div class="form-group @if($errors->first('name')) has-error @endif">
			                {!! Form::label('name', 'Nome:') !!}
			                {!! Form::text('name', (isset($data) ? $data->name : null), ['class' => 'form-control']) !!}
			                <small class="text-danger">{{ $errors->first('name') }}</small>
			            </div>
			            <div class="form-group @if($errors->first('situation')) has-error @endif">
			                {!! Form::label('situation', 'Situação:') !!}
			                {!! Form::select('situation', ['Inativo', 'Ativo'], (isset($data) ? $data->situation : 1), ['id' => 'situation', 'class' => 'form-control', 'required' => 'required']) !!}
			                <small class="text-danger">{{ $errors->first('situation') }}</small>
			            </div>
			        </div>
			        <div class="box-footer">
	                    <div class="form-group btn-group pull-left">
	                        {!! Form::submit("Salvar", ['class' => 'btn btn-primary']) !!}
	                    </div>
	                </div>
		        {!! Form::close() !!}
            </div>
        </div>
        <div class="col-md-6 visible-lg">
            @include('admin.layouts.listForm')
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                language: langPtBr()
            });
        });
    </script>
@endsection
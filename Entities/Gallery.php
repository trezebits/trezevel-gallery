<?php

namespace Modules\Gallery\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Gallery extends Model
{
    use Sluggable;

    protected $dates = [
    	'created_at',
    	'updated_at'
    ];

    protected $fillable = [
        'name',
        'slug',
        'situation'
    ];

    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function images()
    {
        return $this->belongsToMany('Modules\Image\Entities\Image', 'gallery_image');
    }
}

const { mix } = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/assets/js/app.js', 'js/gallery.js')
    .sass( __dirname + '/Resources/assets/sass/app.scss', 'css/gallery.css');

if (mix.inProduction()) {
    mix.version();
}

mix.styles(__dirname + '/Resources/assets/css/dropzone.css', '../../public/css/dropzone.css');
mix.scripts(__dirname + '/Resources/assets/js/dropzone.js', '../../public/js/dropzone.js');
mix.scripts(__dirname + '/Resources/assets/js/dropzone-config.js', '../../public/js/dropzone-config.js');